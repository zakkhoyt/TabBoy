//
//  TabBoyTestViewController.swift
//  ScrollingTabs
//
//  Created by Zakk Hoyt on 2/19/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit
import TabBoy


class TabBoyTestViewController: TabBoyViewController {
    
    private var viewControllers: [UIViewController] = []
    private var numberOfTitles: Int = 3 {
        didSet {
            reloadUI()
        }
    }
    
    private var numberOfViewControllers: Int = 5 {
        didSet {
            reloadUI()
        }
    }
    
    private func reloadUI() {
        
        navigationItem.title = "\(numberOfTitles)/\(numberOfViewControllers)"
        
        
        for viewController in viewControllers {
            viewController.view.removeFromSuperview()
            viewController.removeFromParentViewController()
        }
        viewControllers.removeAll()
        
        for i in 0..<numberOfViewControllers {
            let storyboard = UIStoryboard(name: "ViewController", bundle: Bundle.main)
            let vc = storyboard.instantiateInitialViewController() as! ViewController
            vc.view.backgroundColor = .darkGray
            vc.title = "Screen\(i)"
            vc.label?.text = "Screen\(i)"
            vc.imageView.image = UIImage(named: "iphone_0\(i%4)")
            viewControllers.append(vc)
        }
        reloadViewControllers()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        navigationController?.navigationBar.isHidden = true
        
        dataSource = self
        
        numberOfTitles = 3
        headerPosition = .none
    }
    
    @IBAction func plusBarbAction(_ sender: Any) {
        numberOfTitles += 1
        //        tabBoyViewController.reloadViewControllers()
    }
    @IBAction func minusBarbAction(_ sender: Any) {
        numberOfTitles -= 1
        //        tabBoyViewController.reloadViewControllers()
    }
    
    @IBAction func vcPlusAction(_ sender: Any) {
        numberOfViewControllers += 1
    }
    
    @IBAction func vcMinusAction(_ sender: Any) {
        numberOfViewControllers -= 1
    }
    
}

extension TabBoyTestViewController: TabBoyViewControllerDataSource {
    
    func numberOfViewControllers(_ TabBoyViewController: TabBoyViewController) -> Int {
        return viewControllers.count
    }
    
    func numberOfTitlesVisibile(_ tabBoyViewController: TabBoyViewController) -> Int {
        return numberOfTitles
    }
    
    func tabBoyViewController(_ TabBoyViewController: TabBoyViewController, viewControllerForIndex index: Int) -> UIViewController {
        return viewControllers[index]
    }
    
    func defaultViewController(_ TabBoyViewController: TabBoyViewController) -> UIViewController? {
        return viewControllers[0]
    }
}
