//
//  TabBoyTitleCollectionViewCell.swift
//  ScrollingTabs
//
//  Created by Zakk Hoyt on 2/17/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit


class TabBoyHeaderView: UICollectionReusableView {
    static let identifier = "TabBoyTitleCollectionViewCell"
    
    override var tintColor: UIColor! {
        didSet {
            updateTitles()
        }
    }
    
    var titleTapped: ((_ index: Int) -> Void)?
    
    var barHeight: CGFloat = 2.0
    
    var xProgress: CGFloat = 0 {
        didSet {
            updatePath()
        }
    }
    
    
    private var stackView = UIStackView(frame: .zero)
    
    var titles: [String] = [] {
        didSet {
            updateTitles()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = .white
        if stackView.superview == nil {
            stackView.distribution = .fillEqually
            stackView.axis = .horizontal

            addSubview(stackView)
            
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
            stackView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
            stackView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        }
        
        if bar.superlayer == nil {
            layer.addSublayer(bar)
            bar.fillColor = tintColor.cgColor
        }
    }
    
    private var bar = CAShapeLayer()
    
    private func updateTitles() {
        for arrangedSubview in stackView.arrangedSubviews {
            arrangedSubview.removeFromSuperview()
        }
        
        for (index, title) in titles.enumerated() {
            let button = UIButton(type: .custom)
            button.tag = index
            button.setTitle(title, for: .normal)
            button.setTitleColor(tintColor, for: .normal)
            button.addTarget(self, action: #selector(titleButtonTapped), for: .touchUpInside)
            stackView.addArrangedSubview(button)
        }
        updatePath()
    }
    
    @objc private func titleButtonTapped(sender: UIButton) {
        let index = sender.tag
        titleTapped?(index)
    }
    
    private func updatePath() {
        let x: CGFloat = bounds.width * xProgress
        let y: CGFloat = bounds.height - barHeight
        let divisions = titles.count > 0 ? titles.count : 1
        let width: CGFloat = bounds.width / CGFloat(divisions)
        let height: CGFloat = barHeight
        let rect = CGRect(x: x, y: y, width: width, height: height)
        bar.path = UIBezierPath(roundedRect: rect, cornerRadius: barHeight / 2.0).cgPath
    }
    
}
