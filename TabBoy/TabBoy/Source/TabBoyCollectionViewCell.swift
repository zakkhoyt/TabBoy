//
//  TabBoyColellectionViewCellCollectionViewCell.swift
//  ScrollingTabs
//
//  Created by Zakk Hoyt on 2/17/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

class TabBoyCollectionViewCell: UICollectionViewCell {

    static let identifier = "TabBoyCollectionViewCell"
    var viewController: UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
