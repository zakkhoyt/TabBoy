//
//  TabBoyViewController.swift
//  ScrollingTabs
//
//  Created by Zakk Hoyt on 2/17/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

public protocol TabBoyViewControllerDataSource: class {
    func numberOfViewControllers(_ tabBoyViewController: TabBoyViewController) -> Int
    func numberOfTitlesVisibile(_ tabBoyViewController: TabBoyViewController) -> Int
    func tabBoyViewController(_ tabBoyViewController: TabBoyViewController, viewControllerForIndex index: Int) -> UIViewController
    func defaultViewController(_ tabBoyViewController: TabBoyViewController) -> UIViewController?
}


open class TabBoyViewController: UIViewController {
    
    public var headerPosition: TabBoyHeaderPosition = .top {
        didSet {
            if let layout = collectionView.collectionViewLayout as? TabBoyLayout {
                layout.headerPosition = headerPosition
            }
        }
    }
    
    public var dataSource: TabBoyViewControllerDataSource?
    
    public var tintColor: UIColor = .orange
    
    private var _selectedViewControllerIndex: Int?
    public var selectedViewControllerIndex: Int? {
        set {
            if newValue != _selectedViewControllerIndex {
                _selectedViewControllerIndex = newValue
                if let index = newValue {
                    let indexPath = IndexPath(item: index, section: 0)
                    collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                }
            }
        }
        get {
            return _selectedViewControllerIndex
        }
    }
    
    
    private(set) var collectionView: UICollectionView!
    
    public func reloadViewControllers() {
        collectionView.reloadData()
        
        collectionView.performBatchUpdates({

        }) { [weak self] (finished) in
            self?.selectDefaultViewController()
        }
    }
    
    private func selectDefaultViewController() {
        guard let dataSource = dataSource else { return }
        
        guard let defaultViewController = dataSource.defaultViewController(self) else {
            return
        }
        
        let count = dataSource.numberOfViewControllers(self)
        for i in 0..<count {
            let viewController = dataSource.tabBoyViewController(self, viewControllerForIndex: i)
            if viewController == defaultViewController {
                let indexPath = IndexPath(item: i, section: 0)
                collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                return
            }
        }
    }
    
    
    
    
    private var bundle: Bundle {
        return Bundle(for: TabBoyViewController.self)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        setupViewControllerCollectionView()
    }

    private func setupViewControllerCollectionView() {

        let layout = TabBoyLayout()
        layout.headerHeight = 30
        layout.headerPosition = headerPosition
        layout.dataSource = self
        layout.delegate = self
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.isPagingEnabled = true
        collectionView.frame = view.bounds
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.contentInsetAdjustmentBehavior = .scrollableAxes
        collectionView.dataSource = self
        collectionView.delegate  = self
        
        
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
//        collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
//        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 0).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: 0).isActive = true

        collectionView.register(TabBoyCollectionViewCell.self, forCellWithReuseIdentifier: TabBoyCollectionViewCell.identifier)
        
        collectionView.register(TabBoyHeaderView.self,
                                              forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                              withReuseIdentifier: TabBoyHeaderView.identifier)
    }
    
    
}

extension TabBoyViewController: UICollectionViewDataSource {
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource?.numberOfViewControllers(self) ?? 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TabBoyCollectionViewCell.identifier, for: indexPath) as! TabBoyCollectionViewCell
        if let viewController = dataSource?.tabBoyViewController(self, viewControllerForIndex: indexPath.item) {
            cell.viewController = viewController
            addChildViewController(viewController)
            cell.contentView.addSubview(viewController.view)
            viewController.view.translatesAutoresizingMaskIntoConstraints = false
            viewController.view.topAnchor.constraint(equalTo: cell.contentView.topAnchor).isActive = true
            viewController.view.leftAnchor.constraint(equalTo: cell.contentView.leftAnchor).isActive = true
            viewController.view.rightAnchor.constraint(equalTo: cell.contentView.rightAnchor).isActive = true
            viewController.view.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor).isActive = true
        }
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader,
                                                                   withReuseIdentifier: TabBoyHeaderView.identifier,
                                                                   for: indexPath) as! TabBoyHeaderView
        var titles: [String] = []
        if let dataSource = dataSource {
            for i in 0..<dataSource.numberOfViewControllers(self) {
                let vc = dataSource.tabBoyViewController(self, viewControllerForIndex: i)
                if let title = vc.title {
                    titles.append(title)
                }
            }
            view.titles = titles
        }
        
        view.tintColor = tintColor
        view.titleTapped = { [weak self] (index) in
            self?.collectionView.selectItem(at: IndexPath(item: index, section: 0),
                                            animated: true,
                                            scrollPosition: .centeredHorizontally)
            
        }
        return view
    }
}



extension TabBoyViewController: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }

    public func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TabBoyCollectionViewCell.identifier, for: indexPath) as! TabBoyCollectionViewCell
        if let viewController = cell.viewController {
            viewController.view.removeFromSuperview()
            viewController.removeFromParentViewController()
        }
    }
}

extension TabBoyViewController: TabBoyLayoutDataSource {
    func numberOfTitlesVisibile(_ tabBoyLayout: TabBoyLayout) -> Int {
        return dataSource?.numberOfTitlesVisibile(self) ?? 3
    }
}

extension TabBoyViewController: TabBoyLayoutDelegate {
    func tabBoyLayout(_ tabBoyLayout: TabBoyLayout, didUpdateContentOffsetPercent point: CGPoint, forSupplementaryViewAtIndexPath indexPath: IndexPath) {
        guard let headerView = collectionView.supplementaryView(forElementKind: UICollectionElementKindSectionHeader, at: indexPath) as? TabBoyHeaderView else {
            return
        }
        headerView.xProgress = point.x
    }
}
