//
//  TabBoyCollectionViewLayout.swift
//  ScrollingTabs
//
//  Created by Zakk Hoyt on 2/17/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

protocol TabBoyLayoutDataSource: class {
    func numberOfTitlesVisibile(_ tabBoyLayout: TabBoyLayout) -> Int
}
    
protocol TabBoyLayoutDelegate: class {
    func tabBoyLayout(_ tabBoyLayout: TabBoyLayout, didUpdateContentOffsetPercent point: CGPoint, forSupplementaryViewAtIndexPath indexPath: IndexPath)
}

public enum TabBoyHeaderPosition {
    case none
    case top
    case bottom
}

class TabBoyLayout: UICollectionViewLayout {

    var dataSource: TabBoyLayoutDataSource?
    var delegate: TabBoyLayoutDelegate?
    var headerHeight: CGFloat = 44
    var headerPosition: TabBoyHeaderPosition = .top
    var itemAttributes: [UICollectionViewLayoutAttributes] = []
    var headerAttributes: UICollectionViewLayoutAttributes?
    
    private var headerFrame: CGRect {
        switch headerPosition {
        case .none:
            return .zero
        case .top:
            return CGRect(x: 0, y: 0, width: collectionViewContentSize.width, height: headerHeight)
        case .bottom:
            return CGRect(x: 0, y: 0, width: collectionViewContentSize.width, height: headerHeight)
        }
    }

    override var collectionViewContentSize: CGSize {
        guard let collectionView = collectionView else {
            return .zero
        }
        return CGSize(width: collectionView.bounds.width * CGFloat(itemAttributes.count), height: collectionView.bounds.height)
    }
    
    override func prepare() {
        super.prepare()
        
        guard let collectionView = collectionView,
            let dataSource = collectionView.dataSource,
            let numberOfSections = dataSource.numberOfSections?(in: collectionView) else {
                return
        }

        switch headerPosition {
        case .none:
            break
        case .top:
            let indexPath = IndexPath(item: 0, section: 0)
            headerAttributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, with: indexPath)

            headerAttributes?.frame = CGRect(x: 0,
                                            y: 0,
                                            width: collectionViewContentSize.width,
                                            height: headerHeight)
        case .bottom:
            let indexPath = IndexPath(item: 0, section: 0)
            headerAttributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, with: indexPath)
            headerAttributes?.frame = CGRect(x: 0,
                                            y: collectionView.bounds.height - headerHeight,
                                            width: collectionViewContentSize.width,
                                            height: headerHeight)
        }


        
        itemAttributes.removeAll()
        for s in 0..<numberOfSections {
            for i in 0..<dataSource.collectionView(collectionView, numberOfItemsInSection: s) {
                let indexPath = IndexPath(item: i, section: s)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                
                switch headerPosition {
                case .none:
                    attributes.frame = CGRect(x: CGFloat(i) * collectionView.bounds.width,
                                   y: 0,
                                   width: collectionView.bounds.width,
                                   height: collectionView.bounds.height)

                case .top:
                    attributes.frame = CGRect(x: CGFloat(i) * collectionView.bounds.width,
                                   y: headerHeight,
                                   width: collectionView.bounds.width,
                                   height: collectionView.bounds.height - headerHeight)
                    
                case .bottom:
                    attributes.frame = CGRect(x: CGFloat(i) * collectionView.bounds.width,
                                   y: 0,
                                   width: collectionView.bounds.width,
                                   height: collectionView.bounds.height - headerHeight)
                    
                }
                

                itemAttributes.append(attributes)
            }
        }
    }

    override  func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        guard let collectionView = collectionView else {
                return nil
        }
        updateHeaderAttributes(collectionView: collectionView)
        
        //var output: [UICollectionViewLayoutAttributes] = [headerAttributes]
        var output: [UICollectionViewLayoutAttributes] = []
        
        if let headerAttributes = headerAttributes {
            output.append(headerAttributes)
        }
        
        for attributes in itemAttributes {
            if rect.intersects(attributes.frame) {
                output.append(attributes)
            }
        }
        
        // Update our delegate
        if let delegate = delegate,
            let indexPath = headerAttributes?.indexPath {
            let contentOffsetPercent = CGPoint(x: collectionView.contentOffset.x / collectionViewContentSize.width,
                                               y: collectionView.contentOffset.y / collectionViewContentSize.height)
            delegate.tabBoyLayout(self, didUpdateContentOffsetPercent: contentOffsetPercent, forSupplementaryViewAtIndexPath: indexPath)
        }
        
        return output
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return itemAttributes[indexPath.item]
    }

    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return headerAttributes
    }

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }

    private func updateHeaderAttributes(collectionView: UICollectionView) {
        
        guard let headerAttributes = headerAttributes,
            let dataSource = dataSource else {
            return
        }
        
        // TODO: make public definable
        let itemsPerScreen = min(dataSource.numberOfTitlesVisibile(self), itemAttributes.count)
        let itemWidth = collectionView.bounds.width / CGFloat(itemsPerScreen)
        let width = itemWidth * CGFloat(itemAttributes.count)
        
        switch headerPosition {
        case .none:
            headerAttributes.frame = .zero
        case .top:
            headerAttributes.frame = CGRect(x: 0,
                                            y: 0,
                                            width: width,
                                            height: headerHeight)
        case .bottom:
            headerAttributes.frame = CGRect(x: 0,
                                            y: collectionView.bounds.height - headerHeight,
                                            width: width,
                                            height: headerHeight)
        }
        
        
        let buffer: Int = itemsPerScreen / 2
        let minXProgress: CGFloat = CGFloat(buffer) * collectionView.bounds.width / collectionViewContentSize.width
        let maxXProgress: CGFloat = 1.0 - (1.0 + 1.0/CGFloat(buffer)) * minXProgress
        var xProgress: CGFloat = collectionView.contentOffset.x / collectionViewContentSize.width
        xProgress = max(xProgress, minXProgress)
        xProgress = min(xProgress, maxXProgress)
        let normalizedProgress = xProgress * 2.0 - 1.0 // Change from (0.0 ... value ... 1.0) to (-1.0 ... value ... 1.0)
        let headerContentWidth = (headerAttributes.frame.width) / 2.0
        var x: CGFloat = (collectionView.contentOffset.x + collectionView.bounds.width / 2.0) // centers header above current offset
        if itemsPerScreen >= 3 {
            if itemsPerScreen % 2 == 1 {
                let offsetX = headerContentWidth * normalizedProgress + itemWidth / 2.0
                x -= offsetX
            } else {
                let offsetX = headerContentWidth * normalizedProgress
                x -= offsetX
            }
        }

        print("xProgress: \(xProgress)")
        print("minProgress: \(minXProgress)")
        print("maxProgress: \(maxXProgress)")
        print("normalizedProgress: \(normalizedProgress)")
        print("itemWidth: \(itemWidth)")
        print("headerContentWidth: \(headerContentWidth)")

        switch headerPosition {
        case .none:
            break;
        case .top:
            let y: CGFloat = headerHeight / 2.0
            headerAttributes.center = CGPoint(x: x, y: y)
        case .bottom:
            let y: CGFloat = collectionView.bounds.height - headerHeight / 2.0
            headerAttributes.center = CGPoint(x: x, y: y)
        }
    }

    
    
}
